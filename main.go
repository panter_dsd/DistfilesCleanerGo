package main

import (
	"fmt"
	"strings"
	"bytes"
	"io/ioutil"
	"path/filepath"
	"os"
	"os/exec"
	"sort"
)

type FileList []string

func (s FileList) Len() int {
	return len(s)
}
func (s FileList) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s FileList) Less(i, j int) bool {
	return s[i] < s[j]
}

func ExtractFileName(manifestString string) string {
	if (strings.HasPrefix(manifestString, "DIST")) &&
		(strings.Count(manifestString, " ") > 2) {
		return strings.Split(manifestString, " ")[1]
	}

	return ""
}

func LoadFilesFromManifests(manifestFileName string) FileList {
	fmt.Printf("Parse file %s\n", manifestFileName)

	file_data, _ := ioutil.ReadFile(manifestFileName)
	lines := bytes.Split(file_data, []byte{'\n'})

	var result FileList

	for _, line := range lines {
		fileName := ExtractFileName(string(line))
		if len(fileName) > 0 {
			result = append(result, fileName)
		}
	}

	return result
}

func LoadFilesFromManifestsFolder(folderName string) FileList {
	var result FileList

	CheckFile := func (path string, info os.FileInfo, err error) error {
		if !info.IsDir() && (info.Name() == "Manifest") {
			result = append(result, LoadFilesFromManifests(path)...)
		}
		return nil
	}

	filepath.Walk(folderName, CheckFile)
	return result
}

var emergeInfo = [][]byte{}

func EmergeInfo() [][]byte {
	if len(emergeInfo) > 0 {
		return emergeInfo
	}

	out, err := exec.Command("emerge", []string{"--info"}...).Output()
	if err != nil {
		fmt.Println("Fatal")
		return [][]byte{}
	}

	emergeInfo = bytes.Split(out, []byte{'\n'})
	return emergeInfo
}

func PortageEnv() map[string]string {
	var result = map[string]string{}

	for _, line := range EmergeInfo() {
		if strings.Count(string(line), "=") == 1 {
			splitted := strings.Split(string(line), "=")
			result[splitted[0]] = splitted[1]
		}
	}

	return result
}

func EmergeValue(key string) string {
	return PortageEnv()[key]
}

func ExtractPath(line string) []string {
	result := strings.Split(strings.Trim(line, "\""), " ")
	if (len(result) > 0) && (len(result[0]) == 0) {
		result = []string{}
	}

	return result
}

func OldPortageManifestFolders() []string {
	return append(ExtractPath(EmergeValue("PORTDIR")),
	ExtractPath(EmergeValue("PORTDIR_OVERLAY"))...)
}

func NewPortageManifestFolders() []string {
	var result = []string{}

	for _, line := range EmergeInfo() {
		if strings.Contains(string(line), "location: ") {
			result = append(result, strings.Split(strings.Trim(string(line), " "), " ")[1])
		}
	}

	return result
}

func ManifestsFolders() []string {
	result := OldPortageManifestFolders()

	if len(result) == 0 {
		result = NewPortageManifestFolders()
	}

	return result
}

func LoadFileNames() FileList {
	var result = FileList{}

	for _, folderName := range ManifestsFolders() {
		result = append(result, LoadFilesFromManifestsFolder(folderName)...)
	}

	if len(result) == 0 {
		fmt.Println("Not found manifests")
	}

	return result
}

func DistDir() string {
	return ExtractPath(EmergeValue("DISTDIR"))[0]
}

func FilesForClean() map[string]int64 {
	var result = map[string]int64{}

	fileNames := LoadFileNames()
	sort.Sort(fileNames)

	WalkFunc := func (path string, info os.FileInfo) {
		if !info.IsDir() {
			i := sort.Search(len(fileNames),
							func(i int) bool {
								return fileNames[i] >= info.Name()
							})
			if (i == len(fileNames)) || (fileNames[i] != info.Name()) {
				result[path] = info.Size()
			}
		}
	}

	distDir := DistDir()
	files, _ := ioutil.ReadDir(distDir)

	for _, file := range files {
		WalkFunc(distDir + "/" + file.Name(), file)
	}

	return result
}

func main() {
	fmt.Println(FilesForClean())
}
